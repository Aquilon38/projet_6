<?php

namespace App\Service;

class Notification
{
    
    public function addSuccess($success)
    {
        $_SESSION['success'][]=$success;
    }
        

    public function addError($error)
    {
        $_SESSION['error'][]=$error;
    }
    
    public function getSuccess()
    {
        $success['success'] = null;
                
        if(isset($_SESSION['success'])){

            $success['success'] = $_SESSION['success'];

            unset($_SESSION['success']);

        }
        return $success['success'];
    }

    public function getError()
    {
        $error['error'] = null;
                
        if(isset($_SESSION['error'])){

            $error['error'] = $_SESSION['error'];

            unset($_SESSION['error']);
        }
        return $error['error'];
    }
}
