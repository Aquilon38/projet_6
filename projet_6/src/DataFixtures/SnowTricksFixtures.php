<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\User;
use App\Entity\Media;


class SnowTricksFixtures extends Fixture
{
    
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');
        
        //3 category
        $category = new Category();
        $category->setDifficulty('Facile');
        $manager->persist($category);
        $manager->flush();
        
        $category->setDifficulty('Modéré');
        $manager->persist($category);
        $manager->flush();
        
        $category->setDifficulty('Difficile');
        $manager->persist($category);
        $manager->flush();
        
        //5 user
        for($i = 1; $i <= 10; $i++){
            $user = new User();
            //Mot de passe : "testtest"
            $user->setUserName($faker->userName)
                 ->setPassword('$2y$13$FRd47E8W7/OUA3H7FgVZeukI4dL/dFvoiSdvbDG4CI5Vr4tCVLNn.')
                 ->setCreatedAt($faker->dateTimeBetween('-12 months', '-6 months'))
                 ->setMail($faker->email);
            $manager->persist($user);
          
                     
            // Créer entre 6 et 8 articles
            for($j=1; $j < mt_rand(0, 5); $j++){
                $article = new Article();

                $content = '<p>'.join($faker->paragraphs(5), '</p><p>').'</p>';



                $article->setTitle($faker->sentence)
                        ->setContent($content)
                        ->setPicture($faker->imageUrl())
                        ->setCreatedAt($faker->dateTimeBetween('-6 months', '-3 months'))
                        ->setUser($user)
                        ->setCategory(mt_rand(1, 3));
                $manager->persist($article);

                // On donne des commentaires à l'article
                for($k=1; $k <= mt_rand(0, 5); $k++) {
                    $comment = new Comment();

                    $content = '<p>'.join($faker->paragraphs(2), '</p><p>').'</p>';

                    $days = (new \DateTime())->diff($article->getCreatedAt())->days;

                    $comment->setContent($content)
                            ->setCreatedAt($faker->dateTimeBetween("-".$days." days"))
                            ->setArticle($article)
                            ->setUser($user);

                    $manager->persist($comment);
                }
                
                for($l=1; $l <= mt_rand(1,5); $l++) {
                    $media = new Media();
                    
                    $media->setPhoto($faker->imageUrl())
                          ->setVideo($faker->imageUrl())
                          ->setArticle($article);
                    
                    $manager->persist($media);
                }
            }
        }

        $manager->flush();
    }
}
