<?php //

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ResetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('mail')
//            ->add('createdAt')
//            ->add('password')
//            ->add('userName')
            ->add('reset', TextType::class, ['label' => 'Votre pseudo'])
        ;
    }
}
