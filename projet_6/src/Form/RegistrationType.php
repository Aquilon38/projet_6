<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        if($options['reset']==true){
            $builder
                ->add('password', RepeatedType::class, [
                    'type' => PasswordType::class, 'attr' => [
                    'placeholder' => 'password'],
                    'invalid_message' => 'Les mots de passes ne coïncident pas',
                    'options' => ['attr' => ['class' => 'password-field', 'placeholder' => 'Mot de passe']],
                    'required' => true,
                    'first_options'  => ['label' => 'Votre nouveau mot de passe'],
                    'second_options' => ['label' => 'Confirmation du mot de passe'],
                     ]);
        }
        
        else{
            $builder
                ->add('userName', TextType::class, ['label' => 'Votre pseudo'])
                ->add('mail', EmailType::class, ['label' => 'Votre email'])
                ->add('password', RepeatedType::class, [
                    'type' => PasswordType::class, 'attr' => [
                    'placeholder' => 'password'],
                    'invalid_message' => 'Les mots de passes ne coïncident pas',
                    'options' => ['attr' => ['class' => 'password-field', 'placeholder' => 'Mot de passe']],
                    'required' => true,
                    'first_options'  => ['label' => 'Mot de passe'],
                    'second_options' => ['label' => 'Confirmation du mot de passe'],
                     ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'reset' => false,
        ]);
    }
}
