<?php

namespace App\Form;

use App\Entity\Media;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class MediaPhotoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            
            ->add('photo_file', FileType::class, [
            'label' => 'photo',

            'mapped' => false,

            'required' => true,

            'constraints' => [
                new File([
                    'maxSize' => '3072k',
                    'mimeTypes' => [
                        'image/gif',
                        'image/jpeg',
                        'image/png'
                    ],
                    'mimeTypesMessage' => 'Please upload a valid image document',
                ])
            ],
        ])
        // ...
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Media::class,
        ]);
    }
}
