<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationType;
use App\Form\ResetType;
use App\Repository\UserRepository;
use App\Service\Notification;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/registration", name="registration")
     */
    public function registration(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder)
    {
        $user = new user();
        $user->setCreatedAt(new \DateTime());
        
        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);
            
            $manager->persist($user);
            $manager->flush();
            
            return $this->redirectToRoute('security_login');
        }
        
        return $this->render('security/registration.html.twig', [
            'form'=>$form->createView()
        ]);
    }
    
    /**
     * @Route("/login", name="security_login")
     */
    public function login()
    {
        return $this->render('security/login.html.twig');
    }
    
    /**
     * @Route("/logout", name="security_logout")
     */
    public function logout() {}
    
    
    /**
     * @Route("/forgot_password", name="forgot_password")
     */
    public function  forgotPassword(UserRepository $userRepository, ObjectManager $manager, Request $request)
    {
        //Création des formulaires
        
        $form = $this->createForm(ResetType::class);
        $form->handleRequest($request);
        
        
        try{
        
            if($form->isSubmitted() && $form->isValid()){

                $userToReset = $userRepository->findOneBy(['userName' => $form->getData()]);
                
                if($userToReset){
                    $key = $this->generateRandomString();
                    
                    //Set de la clé reset
                    $userToReset->setReset($key);
                    
                    //Récupération de l'email
                    $mail = $userToReset->getMail();
                    
                    //TODO envoi du mail à l'adresse mail $mail, y attacher le lien de régénération du mot de passe
                    //$link = "http://mjconsultant.org/snowtricks/forgot_password/".$key;
                    
                    $link = "/forgot_password/".$userToReset->getId()."/".$key;
                    
                    $manager->persist($userToReset);
                    $manager->flush();
                    
                    return $this->redirectToRoute('reset_password', [
                        'reset' => $key,
                        'id' => $userToReset->getId(),
                    ]);
                }
                


//                }
//                    $hash = $encoder->encodePassword($user, $user->getPassword());
//                    $user->setReset($hash);
//
//                    $manager->persist($user);
//                    $manager->flush();

                    return $this->redirectToRoute('forgot_password');
//                }
            }
        }
        
        catch(Exception $ex){
            $notification->addError("Une erreur s'est produite");
        }
        
        //1.Identification du pseudo et identification de l'adresse mail
        //2.Envoi du lien de recouvrement du mot de passe
        // Ajout de l'attribut à l'entité User -> Mot de passe unique.
        //3.Dès page ouverte, suppression du lien dans bdd.
        //4.Ajout du nouveau mot de passe et confirmation 
        return $this->render('security/forgotPassword.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    
    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
    /**
     * @Route("/reset_password/{id}/{reset}", name="reset_password")
     */
    public function newPassword(User $user, $reset, Notification $notification, ObjectManager $manager, Request $request, UserPasswordEncoderInterface $encoder) {
        
        try{
                //Condition d'affichage du formulaire
            if($user->getId() && $user->getReset() == $reset){ 

                $form = $this->createForm(RegistrationType::class, $user, $options = ['reset' => true]);
                
                $form->handleRequest($request);
                
                    if($form->isSubmitted() && $form->isValid()){
                        
                        //Hash du mot de passe
                        $hash = $encoder->encodePassword($user, $user->getPassword());
                        $user->setPassword($hash);
                        
                        //Vider le Reset
                        $user->setReset('');
                        
                        $manager->persist($user);
                        $manager->flush();
                        $notification->addSuccess('Nouveau mot de passe effectif');
                        
                        //TODO
                        //Envoi d'un mail de confirmation de changement du mot de passe
                        
                        return $this->redirectToRoute('home');
                    }

                return $this->render('security/resetPassword.html.twig', [
                    'form' => $form->createView(),
                ]);
            }

            else{
                $notification->addError("Lien invalide");
            }
        }
        
        catch(Exception $ex){
            $notification->addError("Une erreur s'est produite");
        }
        
        return $this->redirectToRoute('home', [
            'errors'=>$notification->getError(),
        ]);
        
    }
}
