<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Media;
use App\Entity\User;
use App\Form\ArticleType;
use App\Form\CategoryType;
use App\Form\CommentType;
use App\Form\MediaPhotoType;
use App\Form\MediaVideoType;
use App\Repository\ArticleRepository;
use App\Repository\MediaRepository;
use App\Repository\UserRepository;
use App\Service\FileUploader;
use App\Service\Notification;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Length;

class SnowTricksController extends AbstractController
{
    /**
     * @Route("/snowtricks", name="snow_tricks")
     */
    public function index()
    {
        return $this->render('snow_tricks/index.html.twig', [
            'controller_name' => 'SnowTricksController',
        ]);
    }
    
        /**
         * @Route("/{pageNumber}", name="home") 
         * Liste de tous les articles (snowtricks)
         */
        public function home(ArticleRepository $articleRepository, ObjectManager $manager, Notification $notification, $pageNumber = 1, $display = true){
            
            
            if(!is_numeric($pageNumber)){
                return $this->redirectToRoute('home', [
                    'pageNumber' => 1
                ]);
            }
            
            $allArticles = $articleRepository->findAll();
            
            // test de la fonction repository findBy()
            //Création de la variable PostByDisplay
            //A mettre en fonction
            $postByDisplay =3;
            $articles = $articleRepository->findBy([], ['createdAt' => 'ASC'], $postByDisplay, ($pageNumber-1)*$postByDisplay);
            
            
            
            if(empty($articles)){
                if ($pageNumber === 1){
                    //inscrire une notification
//                    echo 'Aucun article';
                    $notification->addError('Aucun article');
                    $display = null;
                }
                
                else{
                    return $this->redirectToRoute('home', [
                        'pageNumber' => 1
                    ]);
                }
            }
            
    
           
            return $this->render('snow_tricks/home.html.twig', [
                'articles'=>$articles,
                'success'=>$notification->getSuccess(),
                'errors'=>$notification->getError(),
                'display'=>$display,
                'currentPage' => $pageNumber,
                'allArticles' => $allArticles,
                'postByDisplay' => $postByDisplay
            ]);
        }
 
        /**
         * @Route("tricks/details/{id}", name="details")
         * Focus sur un snowtrick
         */
        public function details(Article $trick, ObjectManager $manager, Request $request){

            $user = $this->getUser();
            $editMode = false;
            if($trick && $user && $trick->getUser()->getId() == $user->getId()){
                $editMode = true;
            }
            
            $comment = new Comment();
            $form = $this->createForm(CommentType::class, $comment);
            $form->handleRequest($request);
            
            if($form->isSubmitted() && $form->isValid()){
                $comment->setCreatedAt(new \DateTime())
                        ->setArticle($trick)
                        ->setUser($trick->getUser());
                
                $manager->persist($comment);
                $manager->flush();
                
                return $this->redirectToRoute('details', ['id' => $trick->getId()]);
            }
            

            return $this->render('snow_tricks/details.html.twig', [
                'trick' => $trick,
                'commentForm' => $form->createView(),
                'editMode' => $editMode
            ]);
        }
        
        /**
         * @Route ("tricks/new", name="new")
         * @Route ("tricks/edit_article/{id}", name="edit_article")
         * @IsGranted("ROLE_USER")
         * Création d'article
         */
        public function editArticle(Request $request, ObjectManager $manager, FileUploader $fileUploader, Notification $notification, Article $trick = null, $photo_office = null)
        {
            $user = $this->getUser();
            
            //Si l'user est le créateur du trick
            if($trick && $trick->getUser()->getId() == $user->getId()){
                
                try{
                
                    //Création du formulaire d'ajout de média photo
                    $media = new Media();
                    $formMedia = $this->createForm(MediaPhotoType::class, $media, [
                        'action' => $this->generateUrl('app_media_new', [
                            'id' => $trick->getId()
                        ])
                    ]);

                    //Création du formulaire d'ajout de média vidéo
                    $mediaVideo = new Media();

                    $formMediaVideo = $this->createForm(MediaVideoType::class, $mediaVideo);

                    $formMediaVideo->handleRequest($request);

                    if ($formMediaVideo->isSubmitted() && $formMediaVideo->isValid()){
                        $mediaVideo->setArticle($trick);
                        $manager->persist($mediaVideo);
                        $manager->flush();
                        
                        $notification->addSuccess('Vidéo ajouté');
                        
                        return $this->redirectToRoute('edit_article', [
                        'id' => $trick->getId(), 
                    ]);
                    }


                    //Création du formulaire de l'article

                    $form = $this->createForm(ArticleType::class, $trick);
                    $form->handleRequest($request);

                    if($form->isSubmitted() && $form->isValid()){
                        $trick->setUser($user);

                        $manager->persist($trick);
                        $manager->flush();

                    }
                
                }
                
                catch(Exception $ex){
                    $notification->addError("Une erreur s'est produite");
                }
                
                return $this->render('snow_tricks/article.html.twig', [
                        'formTrick' => $form->createView(),
                        'formMedia' => $formMedia->createView(), 
                        'formMediaVideo' => $formMediaVideo->createView(), 
                        'trick' => $trick,
                        'photo_office' => $photo_office,          
                        'errors' => $notification->getError(),
                        'success' => $notification->getSuccess(),
                    ]);
            }
            
            //Si nouveau trick
            elseif(!$trick){
                
                //Le formulaire indiquera le champs de selection de la photo principale
                
                $trick = new Article();
                $form = $this->createForm(ArticleType::class, $trick, $options = ['photo'=>true]);
                $form->handleRequest($request);
     
                //Premier envoi sur méthode editArticle
                //Puis ajout obligatoire de l'image pour flush et envoi
                
                if($form->isSubmitted() && $form->isValid()){
                    
                        $trick->setCreatedAt(new \DateTime());
                        $trick->setUser($user);

                        //Insertion des conditions d'insertion de la photo dans le fichier ulpoads/photos

                        //Si pas d'envoi côté utilisateur, assignation d'une image d'office
                        if(empty($form['photo_file']->getData())){

                            $photo_office = 'https://fr.wikipedia.org/wiki/Snowboard_freestyle#/media/Fichier:Picswiss_VD-44-23.jpg';

                            $manager->persist($trick);
                            $manager->flush();
                        }

                        else{

                        //Aucune photo d'office
                        $photo_office = null;

                        /** @var UploadedFile $photoFile */
                        $photoFile = $form['photo_file']->getData();

                            try{

                                //Service de chargement du fichier photo
                                if ($photoFile) {
                                    $photoFileName = $fileUploader->upload($photoFile);

                                    $trick->setPicture($photoFileName);

                                    $manager->persist($trick);
                                    $manager->flush();

                                    //Création de l'entité média et mise en lien avec la photo et de l'article
                                    $media = new Media();
                                    $media->setPhoto($photoFileName)
                                          ->setArticle($trick);

                                    //Persit and flush media photo
                                    $manager->persist($media);
                                    $manager->flush();
                                }

                            }

                            catch (Exception $ex){
                                $notification->addError("La photo n'a pas pu être téléchargé");
                            }
                        }

                        $notification->addSuccess("Nouvel article publié");

                    return $this->redirectToRoute('edit_article', [
                        'id' => $trick->getId(), 
                        'photo_office' => $photo_office
                    ]);
                
                
                
            } 
            
        return $this->render('snow_tricks/article.html.twig', [
            'formTrick' => $form->createView(),
            'trick' => $trick,
            'photo_office' => $photo_office,
            'errors' => $notification->getError(),
            'success' => $notification->getSuccess(),
        ]);
        }
    }

    
    /**
     * @Route("/article/{id}/delete", name="delete_article")
     */
    public function deleteArticle(ObjectManager $manager, Article $trick, Notification $notification)
    {
        try{
            //Suppression de l'article
            $manager->remove($trick);
            $manager->flush();

            $notification->addSuccess("L'article a été supprimé");
        }
        
        catch(Exception $ex){
            $notification->addError("Une erreur s'est produite");
        }
        
    return $this->redirectToRoute('home'); 
    }
}