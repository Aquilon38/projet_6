<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Media;
use App\Form\MediaPhotoType;
use App\Repository\ArticleRepository;
use App\Repository\MediaRepository;
use App\Service\FileUploader;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MediaController extends AbstractController
{
    /**
     * @Route("/media", name="media")
     */
    public function index()
    {
        return $this->render('media/index.html.twig', [
            'controller_name' => 'MediaController',
        ]);
    }
    
    /**
     * @Route("/article/{id}/add_media", name="app_media_new")
     * Ajout d'un fichier download
     */
        
    public function uploadPhoto(Request $request, ObjectManager $manager, Article $trick, FileUploader $fileUploader)
    {
        $media = new Media();
        $form = $this->createForm(MediaPhotoType::class, $media);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            
            try{
                /** @var UploadedFile $photoFile */
                $photoFile = $form['photo_file']->getData();

                if ($photoFile) {
                    $photoFileName = $fileUploader->upload($photoFile);

                    // updates the 'photoFilename' property to store the PDF file name
                    // instead of its contents
                    $media->setPhoto($photoFileName);
                    $media->setArticle($trick);

                    // ... persist the $media variable or any other work
                    $manager->persist($media);
                    $manager->flush();


                    if($trick->getPicture() == null){
                        $trick->setPicture($photoFileName);
                        $manager->persist($trick);
                        $manager->flush();
                    }
                    
                    
                }
                $_SESSION['success'][]="Photo téléchargé";
            }
            catch(Exception $ex){
                $_SESSION['errors'][]='Echec de téléchargement de la photo';
            }
        }

        return $this->redirectToRoute('edit_article', [
            'id' => $trick->getId(),
        ]);
    }
    
    /**
     * @Route("/media/{id}/delete", name="delete_media")
     */
    public function deleteMedia(ObjectManager $manager, Media $media, Filesystem $filesystem)
    {
        
        try{
        
            $trick = $media->getArticle();

            //Suppression des fichiers media dans le dossier photos (si il y a)
            $filesystem->remove($this->getParameter('photos_directory').$media->getPhoto());
            
            //Suppression du média
            $manager->remove($media);
            $manager->flush();

            $_SESSION['success'][]="Média supprimé";
        }
        
        catch(Exception $ex){
            //Commentaire à enlever définitivement une fois la partie test fini
//            $_SESSION['errors'][]=$ex->getMessage();
            $_SESSION['errors'][]="Echec de suppression du média";
        }
        
        return $this->redirectToRoute('edit_article', [
            'id' => $trick->getId()
        ]);
    }
    
    /**
     * @Route("/media/{idTrick}/{idMedia}/modify", name="modify_media")
     * Modification de la photo principale, la photo thême de l'article.
     */
    public function modifyMedia(ObjectManager $manager, $idTrick, $idMedia, ArticleRepository $articleRepository, MediaRepository $mediaRepository)
    {
        $trick = $articleRepository->find($idTrick);
        
        $media = $mediaRepository->find($idMedia);
        
        $trick->setPicture($media->getPhoto());
        
        $manager->persist($trick);
        $manager->flush();
        
        $_SESSION['success'][]="Photo de couverture modifiée";
        
        return $this->redirectToRoute('edit_article', [
            'id' => $trick->getId()
        ]);
    }
}
