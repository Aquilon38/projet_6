<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Media;
use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    /**
     * @Route("/testinsert", name="testinsert")
     * Insert de données dans base de donnée via entités pour test
     */
    public function test(ObjectManager $manager)
    {
        
        
        //ajout de donnée à partir de nouvelles entitées
        $user = new User();
        
        $user->setFirstName('Gorges')
             ->setLastName('Delacroix')
             ->setMail('Georgedelacroixxx@gmail.com')
             ->setCreatedAt(new DateTime);
        $manager->persist($user);
       
        $article = new Article();
        
        $article->setTitle('bonjour')
                ->setContent('voici le contenu')
                ->setPicture('http://placehold.it/350x150')
                ->setCreatedAt(new DateTime)
                ->setUser($user);
        $manager->persist($article);
        
        
        $comment = new Comment();
        
        $comment->setContent('voici le contenu')
                ->setCreatedAt(new DateTime)
                ->setUser($user)
                ->setArticle($article);
        $manager->persist($comment);
        
        
        $manager->flush();
        
        
        return $this->render('snow_tricks/testBdd.html.twig', [
            'result'=>$user
        ]);
    }
    
    
    /**
     * @Route("/test", name="test")
     */
    public function index()
    {
        return $this->render('test/index.html.twig', [
            'controller_name' => 'TestController',
        ]);
    }
}
